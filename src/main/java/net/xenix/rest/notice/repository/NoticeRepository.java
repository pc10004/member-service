package net.xenix.rest.notice.repository;

import net.xenix.rest.notice.model.NoticeData;
import net.xenix.spring.helper.annotation.XRepository;
import net.xenix.spring.helper.repository.XAbstractRepository;

@XRepository
public class NoticeRepository extends XAbstractRepository<Integer, NoticeData> {

}
