package net.xenix.rest.notice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.xenix.rest.notice.exception.XNotFoundException;
import net.xenix.rest.notice.model.NoticeData;
import net.xenix.rest.notice.model.RequestData;
import net.xenix.rest.notice.repository.NoticeRepository;
import net.xenix.spring.helper.annotation.XPageNationDefault;
import net.xenix.spring.helper.data.XPage;
import net.xenix.spring.helper.data.XPageNation;
import net.xenix.spring.rest.annotation.XRestAction;

/**
 * @author dudgh
 *
 */
@RestController
public class NoticeController {

	@Autowired
	private NoticeRepository noticeReposit;

	/**
	 * 공지사항 등록
	 * @group 공지사항
	 * @param req 공지사항 데이터
	 * @return 등록 결과
	 * @response 200 등록 완료
	 */
	@RequestMapping(name="공지사항 등록", method=RequestMethod.POST, value="/notices")
	public ResponseEntity<Void> addNotice(@RequestBody RequestData req) {
        NoticeData data = new NoticeData();

		data.setTitle(req.getTitle());
		data.setContent(req.getContent());

		noticeReposit.save(data);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	/**
	 * 공지사항 수정
	 * 
	 * 공지사항
	 * 공지사항
	 * 공지사항
	 * 공지사항
	 * 
	 * @group 공지사항
	 * @param req 수정할 공지사항 데이터
	 * @param noticeId 공지사항 고유 번호
	 * @return 수정 결과
	 * @response 200 수정 완료
	 */
	@RequestMapping(name="공지사항 수정", method=RequestMethod.PUT, value="/notices/{noticeId}")
	public ResponseEntity<Void> updateNotice(@RequestBody RequestData req, @PathVariable("noticeId") Integer noticeId) {

        NoticeData data = new NoticeData();

		data.setNoticeId(noticeId);
		data.setTitle(req.getTitle());
		data.setContent(req.getContent());

		noticeReposit.modifyWithoutNullvalue(data);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	
	/**
	 * 공지사항 리스트
	 * @group 공지사항
	 * @param page 페이지 정보
	 * @return 페이지 목록 데이터
	 */
	@GetMapping(name="공지사항 리스트", value="/notices")
	public XPage<NoticeData> list(@XPageNationDefault(size=10, column="noticeId") XPageNation page) {
		
		return noticeReposit.findAllWithPagenation(page);
	}
	
	
	
	/**
	 * 공지사항 삭제 
	 * @group 공지사항
	 * @param noticeId 공지사항 고유 번호
	 * @return 삭제 결과
	 * @response 200 삭제 완료
	 */
	@RequestMapping(name="공지사항 삭제", method=RequestMethod.DELETE, value="/notices/{noticeId}")
	public ResponseEntity<Void> delete(@PathVariable("noticeId") Integer noticeId) {
		
		noticeReposit.remove(noticeId);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	
	/**
	 * 공지사항 상세 정보 조회
	 * @group 공지사항
	 * @param noticeId 공지사항 고유 번호
	 * @return 공지사항 데이터
	 */
	@RequestMapping(name="공지사항 정보 조회", method=RequestMethod.GET, value="/notices/{noticeId}")
	public NoticeData getContent(@PathVariable("noticeId") Integer noticeId) {
		
		NoticeData result = noticeReposit.findOne(noticeId);
		if(result == null) {
			throw new XNotFoundException(XNotFoundException.NOT_FOUND_NOTICE);
		}
		
		return result;
	}
}
