package net.xenix.rest.notice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.xenix.spring.helper.annotation.XPageNationDefault;
import net.xenix.spring.helper.data.XPage;
import net.xenix.spring.helper.data.XPageNation;

@RestController
public class InfoController {
	
	@Value("${server.port}")
	private int port;
	
	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${spring.application.version}")
	private String version;
	
	/**
	 * 서버정보를 보여준다.
	 * 
	 * @group 서버
	 * @return 서버정보
	 */
	@GetMapping(name="서버정보", value="/info")
	public String info() {
		return applicationName + ":" + port + ":" + version;
	}

}
