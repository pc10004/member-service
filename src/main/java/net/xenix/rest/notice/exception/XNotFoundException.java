package net.xenix.rest.notice.exception;

import org.springframework.http.HttpStatus;

import net.xenix.spring.rest.exception.XRestException;

@SuppressWarnings("serial")
public class XNotFoundException extends XRestException {
	public static final String[] NOT_FOUND_NOTICE = {"40", "공지사항 데이터가 없거나 지정된 고유번호가 잘못 되었습니다."};

	public XNotFoundException() {
		super();
	}
	
	public XNotFoundException(String code, String message) {

		super(code, message);
	}

	public XNotFoundException(String... args) {

		super(args);
	}

	public XNotFoundException(String[] defineError, String outMessage) {

		super(defineError, outMessage);
	}

	@Override
	public HttpStatus exceptionStatus() {
		// TODO Auto-generated method stub
		return HttpStatus.NOT_FOUND;
	}
}
