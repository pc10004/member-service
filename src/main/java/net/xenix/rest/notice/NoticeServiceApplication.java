package net.xenix.rest.notice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@ComponentScan({"net.xenix.spring", "net.xenix.rest.notice"})
@MapperScan("net.xenix.rest.notice")
@EnableCaching
@EnableAsync
@SpringBootApplication
public class NoticeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoticeServiceApplication.class, args);
	}
}
