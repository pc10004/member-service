package net.xenix.rest.notice.model;

import lombok.Data;
import net.xenix.spring.helper.annotation.XColumn;
import net.xenix.spring.helper.annotation.XDefaultValue;
import net.xenix.spring.helper.annotation.XEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 공지사항 데이터 모델
 * @author dev
 */
@Data
public class RequestData {
    /**
     * 제목
     */
    @NotNull
    private String title;
    /**
     * 내용
     */
    private String content;
}
