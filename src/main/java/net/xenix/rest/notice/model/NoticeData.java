package net.xenix.rest.notice.model;

import lombok.Data;
import net.xenix.spring.helper.annotation.XColumn;
import net.xenix.spring.helper.annotation.XDefaultValue;
import net.xenix.spring.helper.annotation.XEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 공지사항 데이터 모델
 * @author dev
 */
@Data
@XEntity("notice")
public class NoticeData {
    /**
     * 공지사항 고유 번호
     */
    @XColumn(isPrimaryKey = true, isAutoIncrement = true)
    private Integer noticeId;
    /**
     * 제목
     */
    @NotNull
    private String title;
    /**
     * 내용
     */
    private String content;
    /**
     * 등록일
     */
    @XDefaultValue(value = "CURRENT_TIMESTAMP")
    private Date regDate;
}
