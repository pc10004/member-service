package net.xenix.rest.notice.mapper;

import org.apache.ibatis.annotations.Mapper;

import net.xenix.rest.notice.model.NoticeData;
import net.xenix.spring.helper.data.XPage;
import net.xenix.spring.helper.data.XPageNation;

@Mapper
public interface NoticeMapper
{
    public int insert(NoticeData notice);

    public int update(NoticeData notide);

    public int delete(int noticeId);

    public NoticeData get(int noticeId);

    public XPage<NoticeData> find(String keyword, XPageNation page);
}
