package net.xenix.rest.notice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles="local")
public class TestXNoticeService {
	private final Logger log = LoggerFactory.getLogger(TestXNoticeService.class);

	private MvcResult mvcResult;
	private String _url = "http://localhost:9988";
	
	@Autowired private MockMvc mockMvc;
	@Autowired private TestRestTemplate testRestTemplate;
	//@Mock private ResumeService resumeService;
    //@Autowired private ResumeCareerRepository resumeCareerRepository;
    @SuppressWarnings("unused")
	@Autowired private WebApplicationContext ctx;
    //@InjectMocks private ServiceReturnJsonController serviceReturnJsonController;

    
//    @Before
//    public void beforeProcess() {
//        this.mockMvc = MockMvcBuilders.standaloneSetup(serviceReturnJsonController)
//        		//.webAppContextSetup(ctx)
//        		.addFilters(new CharacterEncodingFilter("UTF-8", true))
//        		.alwaysDo(print())
//                .build();
//    }

	
    @Test
    public void careerMockMvc() throws Exception {
log.info("Start [ GET Test careerMockMvc... ] ### \n");

		String sRsmNo = "1"; // 5242
		// test 22222354353
		//mvcResult = mockMvc.perform( get("/services/{resNo}" , "1" )
		mvcResult = mockMvc.perform( get("/notices" )
        		//.param("rsmNo", sRsmNo)
				//.contentType(MediaType.APPLICATION_JSON) 
				)
//				.andExpect(
//					(rslt) -> assertTrue(rslt.getResolvedException().getClass().isAssignableFrom(BindException.class))
//				)
//				.andExpect(
//					(rslt) -> assertEquals(
//								rslt.getResolvedException().getClass().getCanonicalName(),
//								BindException.class.getCanonicalName()
//							)
//				)
				.andExpect( status().isOk() )
				//.andExpect(MockMvcResultMatchers.status().is(404))	// 잘못된 uri 접근인 경우에... 
				//.andExpect(jsonPath("$.totalCount",  is(1)) )
				.andReturn() ;
log.info("mvcResult.getResponse().getStatus() >>>>>>>>>>>>>>>>>>>>> " + mvcResult.getResponse().getStatus() );
		//verify(resumeService).testMethod();	// 실행되는 Method chkeck가 필요한 경우에...
log.info("  End  #######################################################################################################################");
    }

}
